#include <iostream>
#include "nodogenerico.h"
using namespace std;
main(){
char resp;
float num;
PilaGenerica<float> pila, pila2, positivos, negativos;
int mayor=-9000000, menor=9000000;

do{
cout<<"Introduzca un numero: ";
cin>>num;
if(num>mayor){
	mayor=num;
}
if(num<menor){
	menor=num;
}

pila.meter(num);
cout<<"Desea introducir otro numero? (S/N)";
cin>>resp;
}while(resp=='s'||resp=='S');

//Mostrar la pila al reves
while(!pila.pilaVacia()){
	num=pila.sacar();
	cout<<num<<endl;
	pila2.meter(num);
	if(num>=0){
	positivos.meter(num);
	}else{
	negativos.meter(num);
	}
}

cout<<"Los numeros positivos son: ";
while(!positivos.pilaVacia()){
	num=positivos.sacar();
	cout<<num<<", ";
}
cout<<endl<<endl;
if(negativos.pilaVacia()){
	cout<<"No hay numeros negativos";
}else{
cout<<"Los numeros negativos son: ";
while(!negativos.pilaVacia()){
	num=negativos.sacar();
	cout<<num<<", ";
}
}
cout<<endl<<endl;
while(!pila2.pilaVacia()){
	pila.meter(pila2.sacar());
}

cout<<"\n\nEl numero mayor es: "<<mayor;
cout<<"\n\nEl numero menor es: "<<menor;
cout<<"\n";
return 0;
}
