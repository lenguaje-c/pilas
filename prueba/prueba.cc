//ALUMNO: AXEL dIAZ
//CEDULA

//*******************ENUNCIADO********************//
/*diseñar un programa que permita cargar datos en 2 pilas:
1era pila: cargar los datos de N empleados:
-CI
-Nombre
-Apellido
-Cargo
-Sueldo
-Descuento

Sueldo total

2da pila:
cargar los datos de N estudiantes
-CI
-Nombre
-Apellido
-Sexo
-Semestre
-Indice Academico

Mostrar los registros de cargados en cada pila
Mostrar la cantidad de empleados menor al promedio de todos los empleados
Mostrar la cantidad de estudiantes con indice academico mayor a 7*/

#include <stdlib.h>
#include <iostream>
#include <string.h>
#include "pilagenerica.h"
using namespace std;

struct Estudiante{
int cedula;
string nombre;
string apellido;
char sexo;
int semestre;
int indice;
};

struct Empleado{
int cedula;
string nombre;
string apellido;
string cargo;
double sueldo;
double descuento;
double sueldototal;
};

main(){
Empleado registrarEm;
Estudiante registrarEs;
char Resp;
double sueldototal, acumsueldos=0;
PilaGenerica<Empleado> pila1, auxemp1;
PilaGenerica<Estudiante> pila2;
int contEmp=0, contest=0, cantemp=0;
double promSueldos=1;

cout<<"\nEMPLEADOS\n";
do{
cout<<"Cedula: ";
cin>>registrarEm.cedula;
cout<<"Nombre: ";
cin>>registrarEm.nombre;
cout<<"Apellido: ";
cin>>registrarEm.apellido;
cout<<"Cargo: ";
cin>>registrarEm.cargo;
cout<<"Sueldo: ";
cin>>registrarEm.sueldo;
cout<<"Descuento: ";
cin>>registrarEm.descuento;
registrarEm.sueldototal = (registrarEm.sueldo) - ( (registrarEm.sueldo)*(registrarEm.descuento) / 100);
acumsueldos=acumsueldos+registrarEm.sueldototal;
pila1.meter(registrarEm);
contEmp++;
promSueldos=acumsueldos/contEmp;
if(registrarEm.sueldototal>promSueldos){
cantemp++;
}

cout<<"Desea introducir otro empleado? (S/N) ";
cin>> Resp;
system("clear");
}while(Resp=='s'||Resp=='S');


//ESTUDIANTES
do{
cout<<"\nESTUDIANTES\n";
cout<<"Cedula: ";
cin>>registrarEs.cedula;
cout<<"Nombre: ";
cin>>registrarEs.nombre;
cout<<"Apellido: ";
cin>>registrarEs.apellido;
cout<<"Sexo: ";
cout<<"\n  1. Femenino";
cout<<"\n  2. Masculino ";
cout<<"\n  Opcion: ";
cin>>registrarEs.sexo;
cout<<"Semestre: ";
cin>>registrarEs.semestre;
cout<<"Indice Academico: ";
cin>>registrarEs.indice;
pila2.meter(registrarEs);
if(registrarEs.indice>7){
contest++;
}
cout<<"Deseas introducir otro estuiante? (S/N) ";
cin>>Resp;
system("clear");
}while(Resp=='S'||Resp=='s');

cout<<"\n\nPILA DE EMPLEADOS: \n\n";
while(!pila1.pilaVacia()){
registrarEm = pila1.sacar();
cout<<"- Cedula: "<<registrarEm.cedula<<", Nombre: "<<registrarEm.nombre<<", Apellido: "<<registrarEm.apellido<<", Cargo: "<<registrarEm.cargo<<", Sueldo: "<<registrarEm.sueldo<<", Descuento: "<<registrarEm.descuento<<", Sueldo Total: "<<registrarEm.sueldototal;
auxemp1.meter(registrarEm);
}



cout<<"\n\nPILA DE ESTUDIANTES: \n";
while(!pila2.pilaVacia()){
registrarEs = pila2.sacar();
cout<<"\n- Cedula: "<<registrarEs.cedula<<", Nombre: "<<registrarEs.nombre<<", Apellido: "<<registrarEs.apellido;
if(registrarEs.sexo==1){ cout<<", Sexo: Femenino";}else{cout<<", Sexo: Masculino";}

cout<<", Semestre: "<<registrarEs.semestre<<", Indice Academico:  "<<registrarEs.indice;

cout<<"\n\nHay "<<contest<<" estudiantes con indice academico mayor a 7";
cout<<endl;

}

cout<<"\nHay "<<cantemp<<" empleados por encima del promedio del sueldo total...";

cout<<"\n\n\nCREADO POR: Axel diaz";
return 0;
}
